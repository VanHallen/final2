// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {

    // code to create new url or increment count goes here
    // (EXAM Q. 12)
    const { app, data } = context;

    const urls = await app.service('urls').find({
      query: {
        url: data.url
      }
    });

    if(urls.total > 0) {
      app.service('urls').patch(urls.data[0]._id, {
        count: urls.data[0].count + 1
      });
    } else {
      app.service('urls').create({
        url: data.url,
        count: 1
      }).then(() => {
        console.log("Added URL: " + data.url + " to the list.");
      })
    }


    // code to fetch url and extract outbound links goes here
    // (EXAM Q. 15)

    return context;
  };
};
