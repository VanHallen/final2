const urls = require('./urls/urls.service.js');
const checkurls = require('./checkurls/checkurls.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(urls);
  app.configure(checkurls);
};
