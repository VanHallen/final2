

const processUrl = require('../../hooks/process-url');

const checkCaptcha = require('../../hooks/check-captcha');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkCaptcha()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [processUrl()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
