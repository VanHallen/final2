const path = require('path');

////const favicon = require('serve-favicon');
////const compress = require('compression');
////const helmet = require('helmet');
////const cors = require('cors');
////const logger = require('./logger');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
///const express = require('@feathersjs/express');



const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const mongoose = require('./mongoose');

////const app = express(feathers());
const app = feathers();

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
////app.use(helmet());
////app.use(cors());
////app.use(compress());
////app.use(express.json());
////app.use(express.urlencoded({ extended: true }));
////app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
////app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
////app.configure(express.rest());


app.configure(mongoose);


// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
////app.use(express.notFound());
////app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

// Set up Google Cloud Functions entry point
exports.feathers = async (req, res) => {
  if ( req.method === "POST" && req.path === '/checkurls' ) {
    /////////////
    // START HERE
    // EXAM Q. 10
    /////////////
    // The statement below currently returns a sample
    // object. You have to replace this statement with the correct
    // sequence of statements that will invoke the proper service
    // and return the result.
    // res.json( {
    //   _id: "testing123",
    //   url: "https//not.a.real.url",
    //   count: 1,
    //   outbound: [
    //     "https://link1.out",
    //     "https://link2.out",
    //     "https://link3.out"
    //   ]
    // } );
    
    const result = await app.service('checkurls').create( req.body );
    res.json( result );

  } else if  ( req.method === "GET" && req.path === '/urls') {
    // EXAM Q. 13
    // The statement below currently returns a sample
    // object. You have to replace this statement with the correct
    // sequence of statements that will invoke the proper service
    // and return the result.
    // res.json( {
    //   data: [
    //       { _id: "one", count: 27, url: "https://first.fake.url"},
    //       { _id: "two", count: 11, url: "https://second.fake.url"},
    //       { _id: "three", count: 4, url: "https://third.fake.url"}
    //   ]
    // } );

    const result = await app.service('urls').find();
    res.json( result );

// DO NOT MAKE ANY CHANGES BELOW HERE
  } else if  ( req.method === "GET" ) {
    // serve static html, js, etc.
    var path = req.path;
    if ( path.endsWith('/')) {
      path = path + 'index.html'
    }
    res.sendFile( __dirname + '/new_public' + path );
  } else {
    console.log( "method: " + req.method );
    console.log( "path: " + req.path );
    res.send('Unsupported request');
  }
};
