(function () {
  'use strict';

  // deal with Google Cloud Functions having a different service prefix
  const getServicePrefix = () => {
      const lastSlashPos = window.location.pathname.lastIndexOf('/');
      if ( lastSlashPos < 1 ) return '';
      else return window.location.pathname.substring( 1, lastSlashPos+1 );
  }

  // add a link to the list -- you can use this later
  const addLink = url => {
    $('#links:last-child').append(
      `<li class="list-group-item">${url}</li>`
    );
  }

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // get handle to service
    const checkurls = app.service( getServicePrefix() + 'checkurls');

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity()) {

            /////////////
            // START HERE
            // EXAM Q. 11
            // EXAM Q. 16
            // EXAM Q. 17
            /////////////
            checkurls.create({
              url: event.target.url.value
            }).then(() => {
              console.log("Checked URL");
            }).catch(error => {
              console.log(error.message);
            })
            
            form.classList.remove('was-validated');
            form.reset();

        } else {
            form.classList.add('was-validated')
        }

      }, false)
    });

  }, false);
}());
